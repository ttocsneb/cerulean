#!venv/Scripts/python

if __name__ == "__main__":
    import sys
    from cerulean import start

    if len(sys.argv) > 1:
        config = sys.argv[1]
    else:
        config = 'config.yml'

    start(config)
