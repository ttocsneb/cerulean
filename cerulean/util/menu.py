import math
import collections


class MenuItem(collections.Sized):

    def __init__(self, text: str, pretext: str = '', posttext: str = '',
                 seperator: str = ' '):
        self.text = text
        self.pretext = pretext
        self.posttext = posttext
        self.seperator = seperator

    def len_pre(self):
        if self.pretext:
            return len(self.pretext) + len(self.seperator)
        return 0

    def len_text(self):
        return len(self.text)

    def len_post(self):
        if self.posttext:
            return len(self.posttext) + len(self.seperator)
        return 0

    def render(self, pre_width: int, post_width: int, edge: bool):

        if self.len_pre():
            pre = self.pretext + self.seperator
            if edge is Menu.INNER:
                pre = '{: >{w}}'.format(pre, w=pre_width)
            else:
                pre = '{:{sep}<{w}}'.format(pre, w=pre_width,
                                            sep=self.seperator)
        else:
            pre = ' ' * pre_width
        if self.len_post():
            post = self.seperator + self.posttext
            if edge is Menu.INNER:
                post = '{: <{w}}'.format(post, w=post_width)
            else:
                post = '{:{sep}>{w}}'.format(post, w=post_width,
                                             sep=self.seperator)
        else:
            post = ' ' * post_width
        return pre + self.text + post

    def __len__(self):
        return self.len_pre() + self.len_text() + self.len_post()


class Menu(collections.MutableSequence):

    LEFT = 0
    CENTER = 1
    RIGHT = 2

    INNER = False
    OUTER = True

    def __init__(self, items: list = list(), aligned: int = CENTER,
                 edge: bool = INNER):
        self._items = list()
        self.extend(items)
        self.aligned = aligned
        self.edge = edge

    def render(self, min_width=0):
        pre, text, post = self._width()
        width = max(min_width, pre + text + post)

        buffer = list()

        if self.aligned is self.__class__.LEFT:
            for i in self:
                buffer.append(i.render(pre, width - pre - i.len_text(),
                              self.edge))
        elif self.aligned is self.__class__.CENTER:
            for i in self:
                w = text / 2 - i.len_text() / 2
                w1 = math.floor(w) + pre
                w2 = math.ceil(w) + post
                buffer.append(i.render(w1,
                                       w2,
                                       self.edge))
        else:
            for i in self:
                buffer.append(i.render(width - post - i.len_text(), post,
                              self.edge))

        return buffer

    def width(self):
        return sum(self._width())

    def _width(self):
        pre_width = 0
        text_width = 0
        post_width = 0

        for i in self:
            pre_width = max(pre_width, i.len_pre())
            text_width = max(text_width, i.len_text())
            post_width = max(post_width, i.len_post())

        return pre_width, text_width, post_width

    def __getitem__(self, index):
        return self._items[index]

    def __delitem__(self, index):
        del self._items[index]

    def __setitem__(self, index, value):
        if not isinstance(value, MenuItem):
            value = MenuItem(str(value))
        self._items[index] = value

    def __len__(self):
        return len(self._items)

    def insert(self, index, value):
        if not isinstance(value, MenuItem):
            value = MenuItem(str(value))
        self._items.insert(index, value)
