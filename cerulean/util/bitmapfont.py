import collections
import pygame

from . import cp437


class Dimension(collections.Sequence):
    """A named tuple of a dimension"""

    def __init__(self, x, y):
        self.__x = x
        self.__y = y

    @property
    def x(self):
        return self.__x

    @property
    def y(self):
        return self.__y

    def __getitem__(self, i):
        return (self.__x, self.__y)[i]

    def __len__(self):
        return 2


class Chars(collections.Sequence):
    """
    Class for a CP437 Font
    """

    def __init__(self, width, height, char_width=0x10, char_height=0x10):

        self._char_width = int(char_width)
        self._char_height = int(char_height)

        self.__width = int(width)
        self.__height = int(height)

        self.__pix_width = self.__width // self._char_width
        self.__pix_height = self.__height // self._char_height

    @property
    def width(self):
        return self.__width

    @property
    def height(self):
        return self.__height

    @property
    def dim(self):
        return Dimension(self.__pix_width, self.__pix_height)

    def __getitem__(self, char):
        if isinstance(char, str):
            char = ord(char)
        else:
            char = int(char)

        # Convert Unicode version to the proper cp437 version
        if char > 0xff:
            char = ord(cp437.getkey(char))

        x = char % self._char_width
        y = char // self._char_width

        if y >= self._char_height:
            raise IndexError

        return Dimension(x * self.__pix_width, y * self.__pix_height)

    def __len__(self):
        return self._char_width * self._char_height


class BitmapFont:

    def __init__(self, file, char_width=0x10, char_height=0x10):
        self._image = pygame.image.load(file)
        self.chars = Chars(self._image.get_width(), self._image.get_height(),
                           char_width, char_height)

    def render(self, text, antialias=False, color=(255, 255, 255),
               background=None):
        if not text:
            return None

        chars = [self.chars[c] for c in text]

        dim = self.chars.dim
        width = len(chars) * dim.x
        height = dim.y

        surf = pygame.Surface((width, height), flags=pygame.SRCALPHA)

        x = 0
        for c in chars:
            surf.blit(self._image, (x, 0, dim.x, dim.y),
                      (c.x, c.y, dim.x, dim.y))
            x += dim.x

        surf.fill(color, special_flags=pygame.BLEND_RGBA_MULT)

        if background:
            newsurf = pygame.Surface((width, height), flags=pygame.SRCALPHA)
            newsurf.fill(background)
            newsurf.blit(surf, (0, 0))
            surf = newsurf

        return surf

    def get_alphabet(self):
        return range(self.chars._char_width * self.chars._char_height)

    def size(self, text):
        return tuple(self.chars.dim)

    def get_linesize(self):
        return self.chars.dim.y

    def get_height(self):
        return self.get_linesize
