import collections
import re
from . import cp437


class Graphic(collections.Sequence):

    regex = re.compile(
        r'\\(?:x([a-fA-F0-9]{2})|([\\abfnrtvABFNRTV]))|([^\\]+)'
    )

    extended = {'a': 7, 'b': 8, 't': 9, 'n': 10, 'v': 11, 'f': 12, 'r': 13,
                '\\': 92}

    def __init__(self, file):
        self._file = file

        self._lines = list()

        # Parse the file
        with open(file, 'r', encoding='utf-8') as f:
            lines = f.readlines()
            for line in lines:
                line = line.rstrip()
                groups = re.findall(self.__class__.regex, line)
                parsed = ''
                # Parse special characters into proper chars
                for group in groups:
                    if group[0]:
                        # Hex character
                        parsed += chr(int(group[0], 16))
                    elif group[1]:
                        # extended character
                        parsed += chr(self.__class__.extended[group[1]])
                    else:
                        # Normal text
                        parsed += group[2]
                self._lines.append(parsed)

        # Find longest line
        max_len = max([len(l) for l in self._lines])
        # Add trailing whitespace to make entire graphic rectangular
        # Also convert text into cp437 unicode
        self._lines = ['{: <{}}'.format(cp437.encode(l), max_len)
                       for l in self._lines]

    def __getitem__(self, index):
        return self._lines[index]

    def __len__(self):
        return len(self._lines)

    @property
    def width(self):
        return len(self[0])

    @property
    def height(self):
        return len(self)

    def render(self):
        return '\n'.join(self)
