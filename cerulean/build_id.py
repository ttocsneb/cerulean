import subprocess

commit = subprocess.run(['git', 'rev-parse', '--short', 'HEAD'],
                        stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
branch = subprocess.run(['git', 'rev-parse', '--abbrev-ref', 'HEAD'],
                        stdout=subprocess.PIPE).stdout.decode('utf-8').rstrip()
