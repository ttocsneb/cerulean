import os
import collections

from marshmallow import Schema, fields, post_load
from ruamel import yaml

# Helper Functions


def merge(orig: dict, new: dict, delete=False, join=True):
    from collections import MutableMapping
    for k, v in orig.items():
        nv = new.get(k)
        # Merge the two values if they are also dicts
        if isinstance(v, MutableMapping) \
                and isinstance(nv, MutableMapping):
            merge(v, nv, delete)
            continue
        # set new value otherwise
        orig[k] = nv

    # Delete extraneous keys from the original
    if delete:
        deletes = set(orig) - set(new)
        for k in deletes:
            del orig[k]

    # Add extraneous keys from the new
    if join:
        joins = set(new) - set(orig)
        for k in joins:
            orig[k] = new[k]

    return orig


# Config Classes


class Dimension(collections.Sequence):

    def __init__(self, data):
        self._data = data

    def __getitem__(self, i):
        return tuple(self._data.values())[i]

    def __len__(self):
        return len(self._data)

    def __getattr__(self, attr):
        return self._data[attr]

    def __setattr__(self, attr, value):
        if attr.startswith('_'):
            object.__setattr__(self, attr, value)
            return
        self._data[attr] = value


class DimensionField(fields.Field):

    def __init__(self, cls_or_instance, dimensions=['x', 'y'], defaults=[],
                 *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._dimensions = dimensions
        self._defaults = defaults
        self._cls = cls_or_instance

    def _cast(self, value, attr, data):
        if isinstance(self._cls, fields.Field):
            return self._cls._deserialize(value, attr, data)
        return self._cls(value)

    def _decast(self, value, attr, obj):
        if isinstance(self._cls, fields.Field):
            return self._cls._serialize(value, attr, obj)

    def _process(self, cast, value, attr, data):

        ret = collections.OrderedDict()

        # Set the defaults
        for i in range(min(len(self._dimensions), len(self._defaults))):
            ret[self._dimensions[i]] = self._defaults[i]

        # Load the data into a dict
        if isinstance(value, collections.Sequence):
            for i in range(min(len(self._dimensions), len(value))):
                ret[self._dimensions[i]] = \
                    cast(value[i], self._dimensions[i], data)
        elif isinstance(value, collections.Mapping):
            for k in self._dimensions:
                if k in value:
                    ret[k] = cast(value[k], k, data)

        return ret

    def _deserialize(self, value, attr, data):
        return Dimension(self._process(self._cast, value, attr, data))

    def _serialize(self, value, attr, obj):
        return self._process(self._decast, value, attr, obj)


class Window(object):
    def __init__(self, size: Dimension, fullscreen: bool = False):
        self.fullscreen = fullscreen
        self.size = size


class Config(object):
    def __init__(self, window: Window, colors: str = None, charmap: str = None,
                 tileset: str = None, numpad: bool = False):

        if not colors:
            colors = 'colors.yml'
        if not charmap:
            charmap = 'charmap.yml'
        if not tileset:
            tileset = 'tileset.yml'

        self.window = window
        self.colors = colors
        self.charmap = charmap
        self.tileset = tileset
        self.data = 'data'
        self.numpad = numpad
        self._file = 'config.yml'
        self._orig = dict()

    def first_time_setup(self, path):
        print("Performing first time setup..")
        import shutil
        curr_dir = os.path.dirname(__file__)
        new_dir = os.path.dirname(path)
        shutil.copyfile(os.path.join(curr_dir, 'defaults/config.yml'),
                        path)
        shutil.copyfile(os.path.join(curr_dir, 'defaults/colors.yml'),
                        os.path.join(new_dir, 'colors.yml'))
        shutil.copyfile(os.path.join(curr_dir, 'defaults/tileset.yml'),
                        os.path.join(new_dir, 'tileset.yml'))
        try:
            shutil.copytree(os.path.join(curr_dir, 'defaults/data'),
                            os.path.join(new_dir, 'data'))
        except FileExistsError:
            print("Skipping creation of '{}'".format(
                os.path.join(new_dir, 'data')))

    def load(self, file=None):
        if file is None:
            file = self._file

        self._file = file

        if not os.path.exists(file):
            self.first_time_setup(file)

        self.data = os.path.join(os.path.dirname(file), 'data')

        schema = ConfigSchema(strict=True)

        with open(file, 'r') as f:
            config = yaml.round_trip_load(f)
        self._orig = config

        data, errors = schema.load(config)

        if len(errors) is not 0:
            print("There were errors while loading '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))
            return False

        self.window = data.window
        self.colors = data.colors
        self.charmap = data.charmap
        self.tileset = data.tileset

        # Load the other configuration files
        folder = os.path.dirname(os.path.abspath(file))

        def parse_path(path):
            if os.path.isabs(path):
                return path
            return os.path.join(folder, path)

        defaults = os.path.join(os.path.dirname(__file__), 'defaults')

        def try_load(path, loader, default):
            if os.path.exists(parse_path(path)):
                loader.load(parse_path(path))
            else:
                loader.load(os.path.join(defaults, default))

        from . import colors, tileset, charmap

        try_load(self.colors, colors.colors, 'colors.yml')
        try_load(self.tileset, tileset.tileset, 'tileset.yml')
        try_load(self.charmap, charmap.charmap, 'charmap.yml')

        # Load charmap defaults
        if self.charmap:
            char_defaults = charmap.CharMap()
            char_defaults.load(os.path.join(os.path.dirname(__file__),
                                            'defaults/charmap.yml'))
            charmap.charmap.set_defaults(char_defaults)

        return True

    def dump(self, file=None):
        if file is None:
            file = self._file

        self._file = file

        schema = ConfigSchema(strict=True)

        data, errors = schema.dump(self)

        if len(errors) is not 0:
            print("There were errors while dumping '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))
            return False

        data = merge(self._orig, data, delete=True)

        with open(file, 'w') as f:
            yaml.round_trip_dump(data, f)


class WindowSchema(Schema):
    fullscreen = fields.Boolean()
    size = DimensionField(fields.Integer(), dimensions=['x', 'y'],
                          defaults=[80, 40])

    @post_load
    def create_window(self, data):
        return Window(**data)


class ConfigSchema(Schema):
    colors = fields.String()
    charmap = fields.String()
    tileset = fields.String()
    numpad = fields.Boolean()

    window = fields.Nested(WindowSchema)

    @post_load
    def create_config(self, data):
        return Config(**data)


config = Config(
    Window(Dimension(collections.OrderedDict(x=80, y=40)))
)
