import os
from marshmallow import Schema, fields, post_load
from ruamel import yaml

from ..util import bitmapfont


class Tileset(object):

    def __init__(self, font: str, tileset: str = None):
        if tileset is None:
            tileset = font
            self._one_font = True
        else:
            self._one_font = font == tileset

        self.tileset = tileset
        self.font = font

        self._tileset = None
        self._font = None

    def load(self, file=None):
        if file is None:
            file = 'tileset.yml'

        schema = TilesetSchema(strict=True)

        with open(file, 'r') as f:
            tileset = yaml.round_trip_load(f)

        data, errors = schema.load(tileset)

        if len(errors) is not 0:
            print("There were errors while loading '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))
            return False

        folder = os.path.dirname(file)

        def parse_path(path):
            if os.path.isabs(path):
                return path
            return os.path.join(folder, path)

        self.tileset = parse_path(data.tileset)
        self.font = parse_path(data.font)
        self._one_font = data._one_font

        print(self.font)
        self._font = bitmapfont.BitmapFont(self.font)
        if not self._one_font:
            self._tileset = bitmapfont.BitmapFont(self.tileset)

        return True

    def get_font(self):
        return self._font

    def get_tileset(self):
        if self._one_font:
            return self._font
        return self._tileset


class TilesetSchema(Schema):
    tileset = fields.String()
    font = fields.String(required=True)

    @post_load
    def create_tileset(self, data):
        return Tileset(**data)


path = os.path.join(os.path.dirname(__file__), 'defaults/data/kaishaku-16.png')
tileset = Tileset(path)
