from marshmallow import Schema, fields, post_load
import re
import collections

from ruamel import yaml

from . import merge


class Color(collections.Sequence):

    LIST = 0
    DICT = 1
    CODE = 2

    def __init__(self, r, g, b, t=LIST):
        self.r = r
        self.g = g
        self.b = b
        self._type = t

    def __getitem__(self, i):
        return [self.r, self.g, self.b][i]

    def __len__(self):
        return 3

    def __repr__(self):
        return str(tuple(self[:]))


class ColorField(fields.Field):

    re_hex = re.compile(r'(?:^#|^0x|^)([\da-f]+)')

    def __init__(self, default=(0, 0, 0), *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default = default

    def _merge_default_color(self, color: list):
        if isinstance(self.default, collections.Sequence):
            merged = list(self.default)
        else:
            merged = [0, 0, 0]

        color = color[:3] + [None] * max(3 - len(color), 0)
        for i, c in enumerate(color[:3]):
            if c is None:
                continue
            merged[i] = c

        return merged

    def _parse_code(self, code: str):
        try:
            code = re.findall(self.__class__.re_hex, code.lower())[0]
        except IndexError:
            return [None, None, None]

        if len(code) is 3:
            code = ''.join([c * 2 for c in code])
        elif len(code) % 2 is 0:
            code = code + code[-2:] * (3 - len(code) // 2)
        elif len(code) is 1:
            code = code * 6
        elif len(code) is 5:
            code = code + code[-1]

        return list(bytes.fromhex(code))

    def _deserialize(self, value, attr, data):
        """
        Accept as many possible inputs as possible. without raising an error

        the accepted formats are:
        list of rgb values
        dict of rgb values
        html color code
        """
        if value is None:
            return self.default
        new_color = list()
        if isinstance(value, collections.Sequence) \
                and not isinstance(value, str):
            # List
            for c in value[:3]:
                try:
                    new_color.append(int(c))
                except ValueError:
                    new_color.append(None)
            t = Color.LIST
        elif isinstance(value, collections.Mapping):
            # Dict
            color = dict()
            for k, v in value.items():
                c = k[0].lower()
                if c in 'rgb':
                    try:
                        color[c] = int(v)
                    except ValueError:
                        pass

            new_color = [color.get(c) for c in ['r', 'g', 'b']]
            t = Color.DICT
        else:
            # color code
            new_color = self._parse_code(str(value))
            t = Color.CODE

        return Color(*self._merge_default_color(new_color), t=t)

    def _serialize(self, value, attr, obj):
        """
        Serialize the ColorObject into its type
        """
        if isinstance(value, Color):
            if value._type == Color.DICT:
                return dict(r=value.r, g=value.g, b=value.b)
            elif value._type == Color.CODE:
                return '#{r:02x}{g:02x}{b:02x}'.format(r=value.r, g=value.g,
                                                       b=value.b)
            else:
                return (value.r, value.g, value.b)
        else:
            return value


class Colors(object):

    color_strings = ('black', 'dark_gray', 'blue', 'light_blue', 'green',
                     'red', 'light_red', 'magenta', 'light_magenta', 'brown',
                     'yellow', 'light_gray', 'white')

    def __init__(self, **kwargs):
        self.black = Color(0, 0, 0, Color.CODE)
        self.dark_gray = Color(0x80, 0x80, 0x80, Color.CODE)
        self.blue = Color(0, 0, 0x80, Color.CODE)
        self.light_blue = Color(0, 0, 0xFF, Color.CODE)
        self.green = Color(0, 0x80, 0, Color.CODE)
        self.light_green = Color(0, 0xFF, 0, Color.CODE)
        self.red = Color(0x80, 0, 0, Color.CODE)
        self.light_red = Color(0xFF, 0, 0, Color.CODE)
        self.magenta = Color(0x80, 0, 0x80, Color.CODE)
        self.light_magenta = Color(0xFF, 0, 0xFF, Color.CODE)
        self.brown = Color(0x80, 0x80, 0, Color.CODE)
        self.yellow = Color(0xFF, 0xFF, 0, Color.CODE)
        self.light_gray = Color(0xC0, 0xC0, 0xC0, Color.CODE)
        self.white = Color(0xFF, 0xFF, 0xFF, Color.CODE)

        for k, v in kwargs.items():
            setattr(self, k, v)

    def _get_color(self, c):
        return tuple(getattr(self, c))

    BLACK = property(lambda s: s._get_color('black'))
    DGRAY = property(lambda s: s._get_color('dark_gray'))
    BLUE = property(lambda s: s._get_color('blue'))
    LBLUE = property(lambda s: s._get_color('light_blue'))
    GREEN = property(lambda s: s._get_color('green'))
    LGREEN = property(lambda s: s._get_color('light_green'))
    RED = property(lambda s: s._get_color('red'))
    LRED = property(lambda s: s._get_color('light_red'))
    MAGENTA = property(lambda s: s._get_color('magenta'))
    LMAGENTA = property(lambda s: s._get_color('light_magenta'))
    BROWN = property(lambda s: s._get_color('brown'))
    YELLOW = property(lambda s: s._get_color('yellow'))
    LGRAY = property(lambda s: s._get_color('light_gray'))
    WHITE = property(lambda s: s._get_color('white'))


class ColorsSchema(Schema):
    black = ColorField(default=(0, 0, 0))
    dark_gray = ColorField(default=(0x80, 0x80, 0x80))
    blue = ColorField(default=(0, 0, 0x80))
    light_blue = ColorField(default=(0, 0, 0xFF))
    green = ColorField(default=(0, 0x80, 0))
    light_green = ColorField(default=(0, 0xFF, 0))
    red = ColorField(default=(0x80, 0, 0))
    light_red = ColorField(default=(0xFF, 0, 0))
    magenta = ColorField(default=(0x80, 0, 0x80))
    light_magenta = ColorField(default=(0xFF, 0, 0xFF))
    brown = ColorField(default=(0x80, 0x80, 0))
    yellow = ColorField(default=(0xFF, 0xFF, 0))
    light_gray = ColorField(default=(0xC0, 0xC0, 0xC0))
    white = ColorField(default=(0xFF, 0xFF, 0xFF))

    @post_load
    def create_colors(self, data):
        return Colors(**data)


class Loader(Colors):
    def __init__(self):
        super().__init__()
        self._colors_raw = dict()

    def load(self, file=None):
        if file is None:
            file = 'colors.yml'
        schema = ColorsSchema(strict=True)

        with open(file, 'r') as f:
            colors = yaml.round_trip_load(f)
            self._colors_raw = colors

        data, errors = schema.load(colors)

        if len(errors) is not 0:
            print("there were errors while loading '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))

        for k, v in vars(data).items():
            setattr(self, k, v)

    def dump(self, file=None):
        if file is None:
            import os
            from os.path import dirname as d
            file = os.path.join(d(d(d(__file__))), 'colors.yml')

        schema = ColorsSchema(strict=True)

        data, errors = schema.dump(self)

        if len(errors) is not 0:
            print("there were errors while dumping '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))

        with open(file, 'w') as f:
            data = merge(self._colors_raw, data, delete=True)
            yaml.round_trip_dump(data, f, indent=2,
                                 block_seq_indent=1)


colors = Loader()
