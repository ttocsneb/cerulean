import collections

from marshmallow import Schema, fields, post_load
from ruamel import yaml

from .colors import colors
from ..util import cp437


class Char(object):
    def __init__(self, id: str, char: int = None, color: str = None,
                 bgcolor: str = None, default=None):
        self.char = char

        self.color = color
        self._default = default
        self.id = id

        def parse_color(color):
            if color is None:
                return None
            color = color.lower()
            if color in colors.color_strings:
                return getattr(colors, color, None)

        self._color = parse_color(color)

    def getChar(self) -> int:
        if self.char is not None:
            return cp437.get(self.char)
        if self._default is not None:
            return self._default.getChar()

    def getColor(self) -> tuple:
        if self._color is not None:
            return tuple(self._color)
        if self._default is not None:
            return self._default.getColor()

    def setParent(self, parent):
        self._default = parent


class CharField(fields.Field):

    def _deserialize(self, value, attr, data):
        if isinstance(value, str):
            if not value.startswith('0x'):
                value = ord(value[0])
            else:
                value = int(value, 16)
        else:
            value = int(value)

        # Make sure the value is between 0 and 255
        return abs(value) & 0xFF


class CharSchema(Schema):
    id = fields.String(required=True)
    char = CharField()
    color = fields.String()

    @post_load
    def create_Char(self, data):
        return Char(**data)


#  Char Mapping
class NestedChar(Char):
    """
    This is returned when using CharMap.x.y.z

    If a char is returned for `y`, then z will not give an error

    This is usefull for when a mapping overrides all submappings.
    """

    def __init__(self, char: Char):
        self.char = char.char
        self.color = char.color
        self._default = char._default
        self._color = char._color

    def __getattr__(self, attr):
        return self


class CharMap(collections.Mapping):
    def __init__(self, maps: list = list(), nested: list = list(),
                 id: str = None):
        self.maps = maps
        self.nested = nested
        self.id = id
        self._default_maps = list()
        self._default_nested = list()

    def load(self, file=None):
        if file is None:
            file = 'charmap.yml'

        schema = CharMapSchema(strict=True)

        with open(file, 'r') as f:
            charmap = yaml.round_trip_load(f)

        data, errors = schema.load(charmap)

        if len(errors) is not 0:
            print("There were errors while loading '%s'", file)
            for k, v in errors.items():
                print("\t%s: %s", str(k), str(v))
            return False

        self.maps = data.maps
        self.nested = data.nested
        self.id = data.id

        return True

    def __getitem__(self, key):
        if isinstance(key, str):
            _keys = key.split('.')
            _key = _keys[0]
            _others = _keys[1:]
        elif isinstance(key, collections.Sequence):
            if len(key) is 0:
                return self
            _key = key[0]
            _others = key[1:]
        else:
            raise NotImplementedError

        for ma in self.maps + self._default_maps:
            if ma.id == _key:
                return ma

        for ne in self.nested + self._default_nested:
            if ne.id == _key:
                return ne[_others]

        raise KeyError

    def __iter__(self):
        for ma in self.maps:
            yield ma
        for ne in self.nested:
            yield ne

    def __len__(self):
        return len(self.maps) + len(self.nested) + len(self._default_maps) \
            + len(self._default_nested)

    def full_iter(self):
        for ma in self.maps:
            yield ma
        for ne in self.nested:
            for sub in ne.full_iter():
                yield ne

    def full_len(self):
        return len(self.maps) + sum([ne.full_len() for ne in self.nested])

    def set_defaults(self, default):
        for i in default:
            if i is None:
                continue
            if isinstance(i, Char):
                if i.id in self:
                    self[i.id].setParent(i)
                else:
                    self._default_maps.append(i)
            else:  # default charmaps
                if i.id in self:
                    nested = self[i.id]
                    if isinstance(nested, CharMap):
                        nested.set_defaults(i)
                else:
                    self._default_nested.append(i)

    def __getattr__(self, attr):
        obj = self[attr]
        if isinstance(obj, Char):
            return NestedChar(obj)
        return obj


class CharMapSchema(Schema):
    maps = fields.Nested(CharSchema, many=True)
    nested = fields.Nested('CharMapSchema', many=True)
    id = fields.String()

    @post_load
    def create_charmap(self, data):
        return CharMap(**data)


charmap = CharMap()
