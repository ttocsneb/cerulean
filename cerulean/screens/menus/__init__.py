import os
from .. import Screen, KeyEventListener
from ...config.colors import colors
from ...config import config

from ...util import graphic, menu

import pygame


class Menu(Screen, KeyEventListener):

    def __init__(self, manager):
        super().__init__(manager)
        self._update = False

        self._title = None

        self._selected = 0

        items = list()
        items.append(menu.MenuItem('Play', '<a>', '<entr>'))
        items.append(menu.MenuItem('Settings', '<s>'))
        items.append(menu.MenuItem('Quit', '<esc>'))

        self._menu = menu.Menu(items, menu.Menu.CENTER, menu.Menu.OUTER)

    def _select(self, num):
        self._menu[self._selected].posttext = ''
        self._menu[num].posttext = '<entr>'
        self._selected = num
        self._update = True

    def keyDown(self, unicode, key, mod):
        if key == pygame.K_ESCAPE \
                or (key == pygame.K_RETURN and self._selected is 2):
            self.manager.exit = True
            self._select(2)

        if key == pygame.K_UP:
            self._select((self._selected - 1) % len(self._menu))
        elif key == pygame.K_DOWN:
            self._select((self._selected + 1) % len(self._menu))

        if unicode == 's' \
                or (key == pygame.K_RETURN and self._selected is 1):
            from . import settings
            self._select(1)
            self.manager.setScreen(settings.Settings(self.manager))

        if unicode == 'a':
            self._select(0)

    def quit(self):
        msg = "\xfePress 'ESC' to close the window!\xfe"

        self.window.cursor = (config.window.size.x // 2 - len(msg) // 2, 12)
        self.window.colors = (colors.LRED, colors.WHITE)
        self.window.write(msg)
        self._update = True

    def update(self, delta: float):
        if self._update:

            # Write Options
            widths = self._menu._width()
            x = config.window.size.x // 2 - widths[1] // 2 - widths[0]
            y = 15

            for i, item in enumerate(self._menu.render()):
                self.window.putchars(
                    item, x, y,
                    colors.BLACK if self._selected is i else colors.WHITE,
                    colors.LGRAY if self._selected is i else colors.BLACK
                )
                y += 3

            # Write Title
            self.window.putchars(
                self._title.render(),
                config.window.size.x // 2 - self._title.width // 2, 3,
                fgcolor=colors.LBLUE, bgcolor=colors.BLACK,
                indent=True)

            # Write Version
            from ... import build_id
            version = "{id.branch} ({id.commit})".format(id=build_id)
            self.window.putchars(version, config.window.size.x - len(version),
                                 config.window.size.y - 1,
                                 colors.DGRAY, colors.BLACK)

            self.window.update()
            self._update = False

    def show(self):
        self.manager.addKeyListener(self)
        self.manager.exit_on_quit = False

        self._title = graphic.Graphic(
            os.path.join(config.data, 'title.txt')
        )

        self.window.fill('\x00', colors.WHITE, colors.BLACK)
        self._update = True

    def hide(self):
        self.manager.removeKeyListener(self)
        del self._title
        self._title = None
