from pymaybe import maybe
from .. import Screen, KeyEventListener
from ...config.colors import colors
from ...config.charmap import charmap
from ...config import config
from ... import config as conf

from ...util import menu, cp437

import pygame


class SettingType:

    BOOL = 0
    INT = 1
    TEXT = 2

    def __init__(self, type, min=None, max=None):
        self._type = type
        self._min = min
        self._max = max

    @property
    def type(self):
        return self._type

    def cast(self, value):
        if self.type is self.BOOL:
            return bool(value)
        if self.type is self.INT:
            value = min(self._max, int(value)) if self._max is not None \
                else int(value)
            value = max(self._min, value) if self._min is not None else value
            return value
        if self.type is self.TEXT:
            return str(value)
        raise NotImplementedError

    def stringify(self, value):
        value = self.cast(value)
        if self.type is self.BOOL:
            return 'yes' if value else 'no'
        return str(value)


class Setting(menu.MenuItem):

    def __init__(self, text, pre, type: SettingType, setting: str,
                 dec=pygame.K_LEFT, inc=pygame.K_RIGHT):

        self._dec = dec
        self._inc = inc
        self._type = type
        self._setting = setting.split('.')

        self._value = self.setting

        super().__init__(text, pre,
                         '[%s]' % self._type.stringify(self._value))

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = self._type.cast(value)
        self.posttext = '[%s]' % self._type.stringify(self._value)

    def key_event(self, unicode, key, mod):
        if self._type.type is SettingType.INT:
            if key is self._dec:
                self.value = self.value - 1
            elif key is self._inc:
                self.value = self.value + 1
        elif self._type.type is SettingType.BOOL:
            if key in [self._dec, self._inc]:
                self.value = not bool(self.value)
        elif self._type.type is SettingType.TEXT:
            if key is pygame.K_BACKSPACE:
                self.value = self.value[:-1]
            if key is not pygame.K_RETURN:
                self.value = self.value + unicode

    @property
    def setting(self):
        sett = config
        for s in self._setting:
            sett = getattr(sett, s)
        return sett


class Settings(Screen, KeyEventListener):

    SCREEN_SIZE = 0

    main_keys = {
        'a': 0,
        's': 1,
        'd': 2,
        'f': 3,
        'g': 4
    }

    sub_keys = {
        'q': 0,
        'w': 1,
        'e': 2,
        'r': 3,
        't': 4
    }

    menus = {
        0: [
            Setting('width', '<q>', SettingType(SettingType.INT, 80),
                    'window.size.x', dec=pygame.K_h, inc=pygame.K_k),
            Setting('height', '<w>', SettingType(SettingType.INT, 40),
                    'window.size.y', dec=pygame.K_h, inc=pygame.K_k)
        ]
    }

    def _set_selected(self, sel):
        self._selected = sel

        self.window.fill(region=(config.window.size.x // 2 + 5, 5,
                                 self._side_settings.width(),
                                 len(self._side_settings) * 2))

        self._side_settings.clear()
        if sel in self.menus:
            self._side_settings.extend(self.menus[sel])
        self._update = True

    def _set_side_selected(self, sel):
        self._side_selected = sel
        self._update = True

    def keyDown(self, unicode, key, mod):
        if key == pygame.K_ESCAPE:
            self.manager.back()

        if key == pygame.K_DOWN:
            self._set_selected((self._selected + 1) % len(self._settings))
        if key == pygame.K_UP:
            self._set_selected((self._selected - 1) % len(self._settings))
        if key == pygame.K_m and len(self._side_settings) > 0:
            self._set_side_selected(
                (self._side_selected + 1) % len(self._side_settings))
        if key == pygame.K_u and len(self._side_settings) > 0:
            self._set_side_selected(
                (self._side_selected - 1) % len(self._side_settings))

        if unicode in self.main_keys \
                and self.main_keys[unicode] < len(self._settings):
            self._set_selected(self.main_keys[unicode])

        if unicode in self.sub_keys \
                and self.sub_keys[unicode] < len(self._side_settings):
            self._set_side_selected(self.sub_keys[unicode])

        if key in [pygame.K_h, pygame.K_k, pygame.K_RIGHT, pygame.K_LEFT]:
            maybe(self._settings)[self._selected].key_event(
                unicode, key, mod)
            maybe(self._side_settings)[self._side_selected].key_event(
                unicode, key, mod)
            self._update = True

        if key == pygame.K_z:
            # Save
            config.window.size.x = self.menus[0][0].value
            config.window.size.y = self.menus[0][1].value
            config.window.fullscreen = self._settings[1].value
            config.numpad = self._settings[2].value

            config.dump()

            self.window.resize(config.window.size.x, config.window.size.y,
                               colors.WHITE, colors.BLACK)
            self.window.fullscreen = config.window.fullscreen

            self._update = True

    def __init__(self, manager):
        super().__init__(manager)

        self._selected = 0
        self._side_selected = 0

        self._settings = menu.Menu(
            [
                menu.MenuItem('screen size', '<a>'),
                Setting('fullscreen', '<s>', SettingType(SettingType.BOOL),
                        'window.fullscreen'),
                Setting('numpad', '<d>', SettingType(SettingType.BOOL),
                        'numpad')
            ], menu.Menu.LEFT, menu.Menu.OUTER
        )

        self._side_settings = menu.Menu(
            self.__class__.menus[0],
            aligned=menu.Menu.LEFT, edge=menu.Menu.OUTER
        )

    def update(self, delta: float):
        if self._update:
            self.window.fill()

            self.window.fill(charmap.wall.vertical.getChar(), colors.DGRAY,
                             region=(config.window.size.x // 2, 0,
                                     1, config.window.size.y))

            x = 5
            y = 5
            width = config.window.size.x // 2 - 10

            self.window.putchars(cp437.decode('select <\x18 \x19 \x1A \x1B>'),
                                 x, y - 3, colors.YELLOW)

            self.window.putchars('apply <z>   back <esc>', 5,
                                 config.window.size.y - 2, colors.YELLOW)

            for i, item in enumerate(self._settings.render(width)):
                self.window.putchars(
                    item,
                    x, y,
                    colors.BLACK if self._selected is i else colors.WHITE,
                    colors.LGRAY if self._selected is i else colors.BLACK
                )
                y += 2

            x = config.window.size.x // 2 + 5
            y = 5

            self.window.putchars('select <u m k h>',
                                 x, y - 3, colors.YELLOW, indent=True)

            for i, item in enumerate(self._side_settings.render(width)):
                self.window.putchars(
                    item,
                    x, y,
                    colors.BLACK if self._side_selected is i else colors.WHITE,
                    colors.LGRAY if self._side_selected is i else colors.BLACK
                )
                y += 2

            self.window.update()
            self._update = False

    def show(self):
        self._update = True
        self.manager.addKeyListener(self)

    def hide(self):
        self.manager.removeKeyListener(self)
