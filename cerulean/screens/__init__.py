import pygcurse
from abc import abstractmethod
from pymaybe import maybe
import pygame


class KeyEventListener:

    def keyDown(self, unicode, key, mod):
        pass

    def keyUp(self, key, mod):
        pass

    def quit(self):
        pass

    def mouseMove(self, pos, rel, buttons):
        pass

    def mouseDown(self, pos, button):
        pass

    def mouseUp(self, pos, button):
        pass


class ScreenManager:

    def __init__(self, window: pygcurse.PygcurseWindow):
        self.__window = window
        self.screens = list()

        self.window.autoupdate = False

        self._changed_screen = None
        self._changed_screen_kwargs = dict()

        self._exit = False
        self._exit_on_back = True
        self._exit_on_quit = True

        self._events = list()

    @property
    def window(self) -> pygcurse.PygcurseWindow:
        return self.__window

    @property
    def currentScreen(self):
        try:
            return self.screens[-1]
        except IndexError:
            return None

    @property
    def exit(self):
        return self._exit

    @exit.setter
    def exit(self, value):
        self._exit = bool(value)

    @property
    def exit_on_back(self):
        return self._exit_on_back

    @exit_on_back.setter
    def exit_on_back(self, value):
        self._exit_on_back = bool(value)

    @property
    def exit_on_quit(self):
        return self._exit_on_quit

    @exit_on_quit.setter
    def exit_on_quit(self, value):
        self._exit_on_quit = bool(value)

    def addKeyListener(self, listener: KeyEventListener):
        if isinstance(listener, KeyEventListener) \
                and listener not in self._events:
            self._events.append(listener)

    def removeKeyListener(self, listener: KeyEventListener):
        if isinstance(listener, KeyEventListener) \
                and listener in self._events:
            self._events.pop(self._events.index(listener))

    def setScreen(self, screen):
        """
        Set a new screen

        Using back will go to the screen before this.
        """
        self._changed_screen = self._setScreen
        self._changed_screen_kwargs = dict(screen=screen)

    def _setScreen(self, screen):
        maybe(self.currentScreen).hide()
        self.screens.append(screen)
        self.currentScreen.show()

    def replaceScreen(self, screen):
        """
        Replace the current screen.

        Using back will not go to the previous screen, but instead to the
        screen before the previous screen.
        """
        self._changed_screen = self._replaceScreen
        self._changed_screen_kwargs = dict(screen=screen)

    def _replaceScreen(self, screen):
        if self.currentScreen:
            self.currentScreen.hide()
            self.screens.pop()
        self.screens.append(screen)
        screen.show()

    def back(self):
        """
        Go to the previous screen.

        If this is the only screen, then it will get restarted.
        """
        self._changed_screen = self._back
        self._changed_screen_kwargs = dict()

    def _back(self):
        maybe(self.currentScreen).hide()
        if len(self.screens) > 1:
            self.screens.pop()
        elif self.exit_on_back:
            self.exit = True
        maybe(self.currentScreen).show()

    def update(self, alpha: float):
        if self._changed_screen:
            self._changed_screen(**self._changed_screen_kwargs)
            self._changed_screen = None
            self._changed_screen_kwargs.clear()

        def forEachEvent(cmd):
            for listener in self._events:
                cmd(listener)

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                forEachEvent(lambda l: l.keyDown(event.unicode, event.key,
                                                 event.mod))
            elif event.type == pygame.KEYUP:
                forEachEvent(lambda l: l.keyUp(event.key, event.mod))
            elif event.type == pygame.MOUSEMOTION:
                forEachEvent(lambda l: l.mouseMove(event.pos, event.rel,
                                                   event.buttons))
            elif event.type == pygame.MOUSEBUTTONDOWN:
                forEachEvent(lambda l: l.mouseDown(event.pos, event.button))
            elif event.type == pygame.MOUSEBUTTONUP:
                forEachEvent(lambda l: l.mouseUp(event.pos, event.button))
            elif event.type == pygame.QUIT:
                forEachEvent(lambda l: l.quit())
                if self.exit_on_quit:
                    self.exit = True

        maybe(self.currentScreen).update(alpha)

    def hide(self):
        maybe(self.currentScreen).hide()


class Screen:

    def __init__(self, manager: ScreenManager):
        self.__manager = manager

    @property
    def manager(self) -> ScreenManager:
        return self.__manager

    @property
    def window(self) -> pygcurse.PygcurseWindow:
        return self.__manager.window

    @abstractmethod
    def update(self, delta: float):
        pass

    @abstractmethod
    def show(self):
        pass

    @abstractmethod
    def hide(self):
        pass
