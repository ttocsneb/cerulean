import time
import pygcurse

from .screens import ScreenManager, menus

from .config import config, tileset

DELTA = 1 / 20


def start(conf):
    config.load(conf)
    window = pygcurse.PygcurseWindow(config.window.size.x,
                                     config.window.size.y,
                                     'Project Cerulean',
                                     tileset.tileset.get_font(),
                                     fullscreen=config.window.fullscreen)
    manager = ScreenManager(window)

    men = menus.Menu(manager)
    manager.setScreen(men)

    while not manager.exit:
        t = time.clock()
        manager.update(DELTA)
        sleep = DELTA - (time.clock() - t)
        if sleep > 0:
            time.sleep(sleep)

    manager.hide()
