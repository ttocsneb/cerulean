import os
from setuptools import setup, find_packages


# with open("README.md", 'r') as fh:
#     LONG_DESCRIPTION = fh.read()


def package_data_dirs(source, sub_folders):
    dirs = []

    for d in sub_folders:
        folder = os.path.join(source, d)
        if not os.path.exists(folder):
            continue

        for dirname, _, files in os.walk(folder):
            dirname = os.path.relpath(dirname, source)
            for f in files:
                dirs.append(os.path.join(dirname, f))

    return dirs


INSTALL_REQUIRES = [
    'pygame',
    'pygcurse',
    'ruamel.yaml',
    'marshmallow',
    'pymaybe',
]

EXTRAS_REQUIRE = {}


def params():
    name = "Cerulean"
    version = '0.0.1'

    description = "A Nethack style dungeon maker"
    # long_description = LONG_DESCRIPTION
    # long_description_content_type = "text/markdown"

    install_requires = INSTALL_REQUIRES
    extras_require = EXTRAS_REQUIRE

    classifiers = [
        "Development Status :: 1 - Planning",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: Implementation :: CPython",
        "Topic :: Games/Entertainment",
    ]
    author = 'Benjamin Jacobs'
    author_email = 'benjammin1100@gmail.com'
    url = 'https://bitbucket.org/ttocsneb/cerulean'

    packages = find_packages()

    include_package_data = True
    zip_safe = False

    entry_points = {
        'console_scripts': [
            'cerulean = cerulean:main'
        ]
    }

    return locals()


setup(**params())
