import unittest

from cerulean.util import menu


class MenuTest(unittest.TestCase):

    def test_menu_item(self):
        menuitem = menu.MenuItem('foobar')

        self.assertEqual(menuitem.len_pre(), 0)
        self.assertEqual(menuitem.len_text(), 6)
        self.assertEqual(menuitem.len_post(), 0)
        self.assertEqual(len(menuitem), 6)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '     foobar     ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         '     foobar     ')

        menuitem.pretext = 'A'

        self.assertEqual(menuitem.len_pre(), 2)
        self.assertEqual(menuitem.len_text(), 6)
        self.assertEqual(menuitem.len_post(), 0)
        self.assertEqual(len(menuitem), 8)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '   A foobar     ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         'A    foobar     ')

        menuitem.posttext = 'B'

        self.assertEqual(menuitem.len_pre(), 2)
        self.assertEqual(menuitem.len_text(), 6)
        self.assertEqual(menuitem.len_post(), 2)
        self.assertEqual(len(menuitem), 10)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '   A foobar B   ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         'A    foobar    B')

        menuitem.seperator = '='
        self.assertEqual(len(menuitem), 10)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '   A=foobar=B   ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         'A====foobar====B')

        menuitem.posttext = ''
        self.assertEqual(len(menuitem), 8)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '   A=foobar     ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         'A====foobar     ')

        menuitem.pretext = ''
        menuitem.posttext = 'B'
        self.assertEqual(len(menuitem), 8)
        self.assertEqual(menuitem.render(5, 5, menu.Menu.INNER),
                         '     foobar=B   ')
        self.assertEqual(menuitem.render(5, 5, menu.Menu.OUTER),
                         '     foobar====B')

    def test_menu(self):
        men = menu.Menu(['asdf', 'qwerty', 'joe'])

        self.assertEqual(men.width(), 6)
        self.assertEqual(type(men[0]), menu.MenuItem)
        self.assertEqual(len(men), 3)
        self.assertEqual(men.render(),
                         [' asdf ',
                          'qwerty',
                          ' joe  '])

        men[0].pretext = '<a>'
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             ' <a> asdf ',
                             '    qwerty',
                             '     joe  '
        ])

        men.aligned = men.LEFT
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             '<a> asdf  ',
                             '    qwerty',
                             '    joe   '
        ])

        men.aligned = men.RIGHT
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             '  <a> asdf',
                             '    qwerty',
                             '       joe'
        ])

        men.edge = men.OUTER
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             '<a>   asdf',
                             '    qwerty',
                             '       joe'
        ])

        men.aligned = men.LEFT
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             '<a> asdf  ',
                             '    qwerty',
                             '    joe   '
        ])

        men.aligned = men.CENTER
        self.assertEqual(men.width(), 10)
        self.assertEqual(men.render(),
                         [
                             '<a>  asdf ',
                             '    qwerty',
                             '     joe  '
        ])

        men[2].posttext = '<AAH>'
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             '<a>  asdf       ',
                             '    qwerty      ',
                             '     joe   <AAH>'
        ])

        men.aligned = men.LEFT
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             '<a> asdf        ',
                             '    qwerty      ',
                             '    joe    <AAH>'
        ])

        men.aligned = men.RIGHT
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             '<a>   asdf      ',
                             '    qwerty      ',
                             '       joe <AAH>'
        ])

        men.edge = men.INNER
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             '  <a> asdf      ',
                             '    qwerty      ',
                             '       joe <AAH>'
        ])

        men.aligned = men.LEFT
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             '<a> asdf        ',
                             '    qwerty      ',
                             '    joe <AAH>   '
        ])

        men.aligned = men.CENTER
        self.assertEqual(men.width(), 16)
        self.assertEqual(men.render(),
                         [
                             ' <a> asdf       ',
                             '    qwerty      ',
                             '     joe <AAH>  '
        ])
