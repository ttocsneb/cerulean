import unittest

from cerulean import screens
import dummy_pygame
dummy_pygame.patch(screens.pygame)

DELTA = 1 / 24


class ScreenTest(screens.Screen):

    def __init__(self, manager: screens.ScreenManager,
                 test: unittest.TestCase, name, ran: dict):
        super().__init__(manager)
        self._test = test
        self._name = name
        self._ran = ran

    def update(self, delta: float):
        self._ran[str(self._name) + '_update'] = True
        self._test.assertEqual(delta, DELTA)

    def show(self):
        self._ran[str(self._name) + '_show'] = True

    def hide(self):
        self._ran[str(self._name) + '_hide'] = True


class ListenerTest(screens.KeyEventListener):

    def __init__(self, ran: dict, name):
        self._ran = ran
        self._name = str(name)

    def keyDown(self, unicode, key, mod):
        self._ran[self._name + '_keyDown'] = (unicode, key, mod)

    def keyUp(self, key, mod):
        self._ran[self._name + '_keyUp'] = (key, mod)

    def quit(self):
        self._ran[self._name + '_quit'] = True

    def mouseMove(self, pos, rel, buttons):
        self._ran[self._name + '_mouseMove'] = (pos, rel, buttons)

    def mouseDown(self, pos, button):
        self._ran[self._name + '_mouseDown'] = (pos, button)

    def mouseUp(self, pos, button):
        self._ran[self._name + '_mouseUp'] = (pos, button)


class TestScreen(unittest.TestCase):

    screen_dict_names = ('show', 'update', 'hide')
    listen_dict_names = ('keyDown', 'keyUp', 'mouseMove', 'mouseDown',
                         'mouseUp', 'quit')

    def assert_functions(self, ran, id, names: list, *args,
                         clear: bool = True):
        def assert_func(token, yes):
            if yes:
                self.assertIn(token, ran)
            else:
                self.assertNotIn(token, ran)

        le = len(args)
        for i, name in enumerate(names):
            if i >= le:
                break
            assert_func('{}_{}'.format(id, name), args[i])

        if clear:
            ran.clear()

    def test_screen_functions(self):
        ran = dict()

        man = screens.ScreenManager(dummy_pygame.create_mock_screen())
        screen1 = ScreenTest(man, self, 1, ran)

        nam = self.__class__.screen_dict_names

        # Check functions of 1 screen
        man.setScreen(screen1)
        self.assert_functions(ran, 1, nam, False, False, False)

        man.update(DELTA)
        self.assert_functions(ran, 1, nam, True, True, False)

        man.update(DELTA)
        self.assert_functions(ran, 1, nam, False, True, False)

        # Check functions of changed Screen
        screen2 = ScreenTest(man, self, 2, ran)
        man.setScreen(screen2)
        self.assert_functions(ran, 2, nam, False, False, False, clear=False)
        self.assert_functions(ran, 1, nam, False, False, False)

        man.update(DELTA)
        self.assert_functions(ran, 2, nam, True, True, False, clear=False)
        self.assert_functions(ran, 1, nam, False, False, True)

        man.update(DELTA)
        self.assert_functions(ran, 2, nam, False, True, False, clear=False)
        self.assert_functions(ran, 1, nam, False, False, False)

        # Check functions of replaced Screen
        screen3 = ScreenTest(man, self, 3, ran)
        man.replaceScreen(screen3)
        self.assert_functions(ran, 3, nam, False, False, False, clear=False)
        self.assert_functions(ran, 2, nam, False, False, False)

        man.update(DELTA)
        self.assert_functions(ran, 3, nam, True, True, False, clear=False)
        self.assert_functions(ran, 2, nam, False, False, True)

        # Check the back function
        man.back()
        self.assert_functions(ran, 3, nam, False, False, False, clear=False)
        self.assert_functions(ran, 2, nam, False, False, False, clear=False)
        self.assert_functions(ran, 1, nam, False, False, False)

        man.update(DELTA)
        self.assert_functions(ran, 3, nam, False, False, True, clear=False)
        self.assert_functions(ran, 2, nam, False, False, False, clear=False)
        self.assert_functions(ran, 1, nam, True, True, False)

        # Check the back function doesn't do anything when only 1 screen
        man.back()
        man.update(DELTA)
        self.assert_functions(ran, 1, nam, True, True, True)

        man.hide()
        self.assert_functions(ran, 1, nam, False, False, True)

    def test_exit_on_back(self):
        ran = dict()

        man = screens.ScreenManager(dummy_pygame.create_mock_screen())
        screen1 = ScreenTest(man, self, 1, ran)

        nam = self.__class__.screen_dict_names

        # Test that exit will not be true when back is used too many times
        man.exit_on_back = False
        man.setScreen(screen1)
        man.update(DELTA)
        self.assert_functions(ran, 1, nam, True, True, False)
        self.assertEqual(man.exit, False)

        screen2 = ScreenTest(man, self, 2, ran)
        man.setScreen(screen2)
        man.update(DELTA)
        self.assert_functions(ran, 2, nam, True, True, False)
        self.assertEqual(man.exit, False)

        for i in range(2):
            man.back()
            man.update(DELTA)
            self.assertEqual(man.exit, False)

        # Test that exit will be true when back is used too many times
        man.exit_on_back = True
        ran.clear()
        man.setScreen(screen2)
        man.update(DELTA)
        self.assert_functions(ran, 2, nam, True, True, False)
        self.assertEqual(man.exit, False)

        for i in [False, True]:
            man.back()
            man.update(DELTA)
            self.assertEqual(man.exit, i)

    def test_key_listeners(self):
        import pygame

        ran = dict()
        man = screens.ScreenManager(dummy_pygame.create_mock_screen())

        lin = self.__class__.listen_dict_names

        # Test basic events
        man.addKeyListener(ListenerTest(ran, 1))
        self.assert_functions(ran, 1, lin, *((False,) * len(lin)))

        man.update(DELTA)
        self.assert_functions(ran, 1, lin, *((False,) * len(lin)))

        key_events = (
            dict(
                type=pygame.KEYDOWN, unicode='a', key=45, mod=None
            ),
            dict(
                type=pygame.KEYUP, key=119, mod=45
            ),
            dict(
                type=pygame.MOUSEMOTION, pos=(5, 5), rel=(-45, 5), buttons=()
            ),
            dict(
                type=pygame.MOUSEBUTTONDOWN, pos=(45, 12), button=1
            ),
            dict(
                type=pygame.MOUSEBUTTONUP, pos=(112, 0), button=0
            ),
            dict(
                type=pygame.QUIT
            )
        )

        # Test each event individually
        for i, devent in enumerate(key_events):
            dummy_pygame.event.add_event(dummy_pygame.EventObj(**devent))
            man.update(DELTA)

            vals = [False] * i
            vals += [True]
            vals += [False] * (len(lin) - i - 1)
            self.assert_functions(ran, 1, lin, *vals)

        # Test each event at the same time
        for devent in key_events:
            dummy_pygame.event.add_event(dummy_pygame.EventObj(**devent))
        man.update(DELTA)
        self.assert_functions(ran, 1, lin, *((True,) * len(lin)))

    def test_exit_on_quit(self):
        import pygame

        ran = dict()
        man = screens.ScreenManager(dummy_pygame.create_mock_screen())

        lin = self.__class__.listen_dict_names

        man.addKeyListener(ListenerTest(ran, 1))
        self.assert_functions(ran, 1, lin, *((False,) * len(lin)))

        # Check that quit event does not exit if exit_on_quit is False
        man.exit_on_quit = False
        equit = dummy_pygame.EventObj(pygame.QUIT)
        dummy_pygame.event.add_event(equit)
        man.update(DELTA)
        self.assert_functions(ran, 1, ['quit'], True)
        self.assertEqual(man.exit, False)

        # Check that quit event does exit when exit_on_quit is True
        man.exit_on_quit = True
        dummy_pygame.event.add_event(equit)
        man.update(DELTA)
        self.assert_functions(ran, 1, ['quit'], True)
        self.assertEqual(man.exit, True)
