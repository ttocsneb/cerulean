import os
import unittest

from os.path import dirname as d

config = os.path.join(d(os.path.realpath(__file__)), 'config')

config_1 = os.path.join(config, 'config_2.yml')
config_2 = os.path.join(config, 'config_2.yml')

qwerty = os.path.join(config, 'qwerty.png')
foobar = os.path.join(config, 'foobar.png')

from cerulean.config import config, colors, tileset
from cerulean.config.charmap import charmap, Char, CharMap


class TestSetings(unittest.TestCase):

    def assertClass(self, obj, cls):
        self.assertTrue(isinstance(obj, cls), msg="{} is not a {}".format(
            type(obj), cls)
        )

    def test_config_1(self):
        config.load(config_1)

        # Test Config exclusive settings
        self.assertEqual(config.window.fullscreen, True)
        self.assertEqual(config.window.size[:], (84, 25))

        # Test that Colors have loaded
        self.assertEqual(colors.colors.BLACK, (0x11, 0x11, 0x11))
        self.assertEqual(colors.colors.DGRAY, (0x88, 0x88, 0x88))
        self.assertEqual(colors.colors.BLUE, (0, 0, 0x88))
        self.assertEqual(colors.colors.LBLUE, (0, 0, 0xAA))
        self.assertEqual(colors.colors.GREEN, (0, 0x88, 0))
        self.assertEqual(colors.colors.LGREEN, (0, 0xAA, 0))
        self.assertEqual(colors.colors.RED, (0x88, 0, 0))
        self.assertEqual(colors.colors.LRED, (0xAA, 0, 0))
        self.assertEqual(colors.colors.MAGENTA, (0x88, 0, 0x88))
        self.assertEqual(colors.colors.LMAGENTA, (0xAA, 0, 0xAA))
        self.assertEqual(colors.colors.BROWN, (0x88, 0x88, 0))
        self.assertEqual(colors.colors.YELLOW, (0xAA, 0xAA, 0))
        self.assertEqual(colors.colors.LGRAY, (0xCC, 0xCC, 0xCC))
        self.assertEqual(colors.colors.WHITE, (0xAA, 0xAA, 0xAA))

        # Test That Charmap has loaded

        # Assert dict function
        self.assertIn('foo', charmap)
        self.assertIn('bar', charmap)
        self.assertIn('qwert', charmap)
        self.assertIn('nest.yak', charmap)

        # Assert attribute existance
        self.assertClass(charmap.foo, Char)
        self.assertClass(charmap.bar, Char)
        self.assertClass(charmap.qwert, Char)
        self.assertClass(charmap.nest, CharMap)
        self.assertClass(charmap.nest.yak, Char)

        # Assert Char Value
        self.assertEqual(charmap.foo.getChar(), chr(0x45))
        self.assertEqual(charmap.bar.getChar(), chr(0x61))
        self.assertEqual(charmap.qwert.getChar(), chr(0x30))
        self.assertEqual(charmap.nest.yak.getChar(), chr(0x00))

        # Assert Char Color
        self.assertEqual(charmap.foo.getColor(), (0xaa,) * 3)
        self.assertEqual(charmap.bar.getColor(), (0x88,) * 3)
        self.assertEqual(charmap.qwert.getColor(), (0x88, 0, 0x88))
        self.assertEqual(charmap.nest.yak.getColor(), (0x88, 0x88, 0))

        # Test That Tileset has been loaded

        self.assertEqual(tileset.tileset.font, qwerty)
        self.assertEqual(tileset.tileset.tileset, foobar)

    def test_config_2(self):
        config.load(config_1)

        # Test Config exclusive settings
        self.assertEqual(config.window.fullscreen, True)
        self.assertEqual(config.window.size[:], (84, 25))
