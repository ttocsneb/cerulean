import os
import unittest

from os.path import dirname as d

config = os.path.join(d(os.path.realpath(__file__)), 'config')

charmap_1 = os.path.join(config, 'charmap_1.yml')
charmap_2 = os.path.join(config, 'charmap_2.yml')
charmap_3 = os.path.join(config, 'charmap_3.yml')
charmap_31 = os.path.join(config, 'charmap_31.yml')

from cerulean.config.charmap import CharMap, charmap, Char
from cerulean.util import cp437


class TestCharmapConfig(unittest.TestCase):

    def assertClass(self, obj, cls):
        self.assertTrue(isinstance(obj, cls), msg="{} is not a {}".format(
            type(obj), cls)
        )

    def test_charmap_1(self):
        charmap.load(charmap_1)

        # Assert dict function
        self.assertIn('foo', charmap)
        self.assertIn('bar', charmap)
        self.assertIn('qwert', charmap)
        self.assertIn('nest.yak', charmap)

        # Assert attribute existance
        self.assertClass(charmap.foo, Char)
        self.assertClass(charmap.bar, Char)
        self.assertClass(charmap.qwert, Char)
        self.assertClass(charmap.nest, CharMap)
        self.assertClass(charmap.nest.yak, Char)

        # Assert Char Value
        self.assertEqual(charmap.foo.getChar(), cp437.get(0x45))
        self.assertEqual(charmap.bar.getChar(), cp437.get(0x61))
        self.assertEqual(charmap.qwert.getChar(), cp437.get(0x30))
        self.assertEqual(charmap.nest.yak.getChar(), cp437.get(0x00))

        # Assert Char Color
        self.assertEqual(charmap.foo.getColor(), (0xff,) * 3)
        self.assertEqual(charmap.bar.getColor(), (0x80,) * 3)
        self.assertEqual(charmap.qwert.getColor(), (0x80, 0, 0x80))
        self.assertEqual(charmap.nest.yak.getColor(), (0x80, 0x80, 0))

    def test_charmap_2(self):
        charmap.load(charmap_2)

        # Assert attribute existance
        self.assertClass(charmap.foo.bar, Char)
        self.assertClass(charmap.foo.bamf, Char)
        self.assertClass(charmap.jeff.bob, Char)
        self.assertClass(charmap.jeff.emp, Char)
        self.assertClass(charmap.jeff.ty, Char)

        # Assert Char characters
        self.assertEqual(charmap.foo.bar.getChar(), cp437.get(0x34))
        self.assertEqual(charmap.foo.bamf.getChar(), cp437.get(0xC8))
        self.assertEqual(charmap.jeff.bob.getChar(), cp437.get(0x45))
        self.assertEqual(charmap.jeff.emp.getChar(), None)
        self.assertEqual(charmap.jeff.ty.getChar(), cp437.get(0x54))

        # Assert Char Color
        self.assertEqual(charmap.foo.bar.getColor(), (0xFF, 0xFF, 0xFF))
        self.assertEqual(charmap.foo.bamf.getColor(), (0xFF, 0xFF, 0xFF))
        self.assertEqual(charmap.jeff.bob.getColor(), None)
        self.assertEqual(charmap.jeff.emp.getColor(), (0xC0, 0xC0, 0xC0))
        self.assertEqual(charmap.jeff.ty.getColor(), None)

    def test_charmap_3(self):
        charmap.load(charmap_3)

        defaults = CharMap()
        defaults.load(charmap_31)

        charmap.set_defaults(defaults)

        # Assert Existances
        self.assertClass(charmap.bar, Char)
        self.assertClass(charmap.foo, Char)
        self.assertClass(charmap.bat, Char)
        self.assertClass(charmap.still, Char)
        self.assertClass(charmap.still.here, Char)
        self.assertClass(charmap.nest.qwer, Char)
        self.assertClass(charmap.nest.tye, Char)
        self.assertClass(charmap.nest.foobar.bird, Char)
        self.assertClass(charmap.nest.foobar.bye, Char)
        self.assertClass(charmap.nest.foobar.blue, Char)
        self.assertClass(charmap.nest.nest2.electric, Char)
        self.assertClass(charmap.you.me, Char)

        # Assert Overriden Values
        self.assertEqual(charmap.bar.getChar(), cp437.get(0x45))
        self.assertEqual(charmap.bar.getColor(), (0x80, 0x80, 0))
        self.assertEqual(charmap.foo.getChar(), cp437.get(0x45))
        self.assertEqual(charmap.foo.getColor(), (0xff,) * 3)
        self.assertEqual(charmap.bat.getChar(), cp437.get(0xA2))
        self.assertEqual(charmap.still.here.getChar(), 'h')
        self.assertEqual(charmap.still.getColor(), (0, 0, 0x80))
        self.assertEqual(charmap.nest.qwer.getChar(), cp437.get(0x2F))
        self.assertEqual(charmap.nest.foobar.bye.getChar(), cp437.get(0x56))
        self.assertEqual(charmap.nest.foobar.bird.getChar(), cp437.get(0x60))
        self.assertEqual(charmap.nest.nest2.electric.getChar(), '2')
        self.assertEqual(charmap.you.me.getChar(), 'm')
