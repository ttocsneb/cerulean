import os
import unittest

from os.path import dirname as d

di = d(os.path.realpath(__file__))

colors_1 = os.path.join(di, 'config', 'colors_1.yml')
colors_2 = os.path.join(di, 'config', 'colors_2.yml')
colors_3 = os.path.join(di, 'config', 'colors_3.yml')

# Import cerulean
from cerulean.config.colors import colors


class TestColorsConfig(unittest.TestCase):

    def test_colors_1(self):
        colors.load(colors_1)

        self.assertEqual(colors.BLACK, (0x11, 0x11, 0x11))
        self.assertEqual(colors.DGRAY, (0x88, 0x88, 0x88))
        self.assertEqual(colors.BLUE, (0, 0, 136))
        self.assertEqual(colors.LBLUE, (0, 0, 0xAA))
        self.assertEqual(colors.GREEN, (0, 136, 0))
        self.assertEqual(colors.LGREEN, (0, 0xAA, 0))
        self.assertEqual(colors.RED, (136, 0, 0))
        self.assertEqual(colors.LRED, (160, 0, 0))
        self.assertEqual(colors.MAGENTA, (0x88, 0, 0x88))
        self.assertEqual(colors.LMAGENTA, (160, 0, 160))
        self.assertEqual(colors.BROWN, (0x88, 0x88, 0))
        self.assertEqual(colors.YELLOW, (0xAA, 0xAA, 0))
        self.assertEqual(colors.LGRAY, (0xCC, 0xCC, 0xCC))
        self.assertEqual(colors.WHITE, (0xAA, 0xAA, 0xAA))

    def test_colors_2(self):
        colors.load(colors_2)

        self.assertEqual(colors.BLACK, (0x11, 0x11, 0x11))
        self.assertEqual(colors.DGRAY, (0x80, 0x80, 0x80))
        self.assertEqual(colors.BLUE, (0, 0, 136))
        self.assertEqual(colors.LBLUE, (0, 0, 0xAA))
        self.assertEqual(colors.GREEN, (0, 128, 0))
        self.assertEqual(colors.LGREEN, (0, 0, 0))
        self.assertEqual(colors.RED, (136, 0, 0))
        self.assertEqual(colors.LRED, (0xCC, 0, 0))
        self.assertEqual(colors.MAGENTA, (0x88, 0, 0x88))
        self.assertEqual(colors.LMAGENTA, (0xA0, 0xAA, 0xAA))
        self.assertEqual(colors.BROWN, (0x88, 0x88, 0))
        self.assertEqual(colors.YELLOW, (0xCC, 0xCC, 0))
        self.assertEqual(colors.LGRAY, (0xCC, 0xCC, 0xCC))
        self.assertEqual(colors.WHITE, (0xFF, 0xFF, 0xFF))

    def test_colors_3(self):
        colors.load(colors_3)

        self.assertEqual(colors.BLACK, (0x11, 0x11, 0x11))
        self.assertEqual(colors.DGRAY, (0x88, 0x88, 0x88))
        self.assertEqual(colors.BLUE, (0, 0, 0x88))
        self.assertEqual(colors.LBLUE, (0, 0, 0xAA))
        self.assertEqual(colors.GREEN, (0, 0x88, 0))
        self.assertEqual(colors.LGREEN, (0, 0xAA, 0))
        self.assertEqual(colors.RED, (0x88, 0, 0))
        self.assertEqual(colors.LRED, (0xAA, 0, 0))
        self.assertEqual(colors.MAGENTA, (0x88, 0, 0x88))
        self.assertEqual(colors.LMAGENTA, (0xAA, 0, 0xAA))
        self.assertEqual(colors.BROWN, (0x88, 0x88, 0))
        self.assertEqual(colors.YELLOW, (0xAA, 0xAA, 0))
        self.assertEqual(colors.LGRAY, (0xCC, 0xCC, 0xCC))
        self.assertEqual(colors.WHITE, (0xAA, 0xAA, 0xAA))
