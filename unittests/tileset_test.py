import os
import unittest

from os.path import dirname as d

config = os.path.join(d(os.path.realpath(__file__)), 'config')

tile_1 = os.path.join(config, 'tileset_1.yml')
tile_2 = os.path.join(config, 'tileset_2.yml')
tile_3 = os.path.join(config, 'tileset_3.yml')

qwerty = os.path.join(config, 'qwerty.png')
foobar = os.path.join(config, 'foobar.png')

from cerulean.config.tileset import tileset
from marshmallow.exceptions import ValidationError


class TestTileSetConfig(unittest.TestCase):

    def test_tileset_1(self):
        tileset.load(tile_1)

        self.assertEqual(tileset.font, qwerty)
        self.assertEqual(tileset.tileset, foobar)

    def test_tileset_2(self):
        self.assertRaisesRegex(ValidationError, r'font[\w\W]*Missing data',
                               tileset.load, tile_2)

    def test_tileset_3(self):
        tileset.load(tile_3)

        self.assertEqual(tileset.font, qwerty)
        self.assertEqual(tileset.tileset, qwerty)
