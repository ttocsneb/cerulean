from unittest import mock


def create_mock_screen():
        window = mock.MagicMock()
        window.autoupdate = True
        return window


class EventObj:

    def __init__(self, type: int, **kwargs):
        self.type = type

        for k, v in kwargs.items():
            setattr(self, k, v)


class Event:

    def __init__(self):
        self._events = list()

    def get(self):
        events = self._events[:]
        self._events.clear()
        return events

    def add_event(self, item):
        self._events.append(item)


event = Event()


def patch(pygame):
    global event
    pygame.event = event
